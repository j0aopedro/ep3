class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nome
      t.string :email
      t.string :senha
      t.string :local
      t.text :bio

      t.timestamps null: false
    end
  end
end
