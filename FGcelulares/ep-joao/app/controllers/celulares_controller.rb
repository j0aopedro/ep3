class CelularesController < ApplicationController
  before_action :set_celulares, only: [:show, :edit, :update, :destroy]

  # GET /celulares
  # GET /celulares.json
  def index
    @celulares = Celulare.all
  end

  # GET /celulares/1
  # GET /celulares/1.json
  def show
  end

  # GET /celulares/new
  def new
    @celulare = Celulare.new
  end

  # GET /celulares/1/edit
  def edit
  end

  # POST /celulares
  # POST /celulares.json
  def create
    @celulares = Celulare.new(celulares_params)

    respond_to do |format|
      if @celulares.save
        format.html { redirect_to @celulares, notice: 'Celulares was successfully created.' }
        format.json { render :show, status: :created, location: @celulares }
      else
        format.html { render :new }
        format.json { render json: @celulares.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /celulares/1
  # PATCH/PUT /celulares/1.json
  def update
    respond_to do |format|
      if @celulares.update(celulare_params)
        format.html { redirect_to @celulares, notice: 'Celulares was successfully updated.' }
        format.json { render :show, status: :ok, location: @celulares }
      else
        format.html { render :edit }
        format.json { render json: @celulares.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /celulares/1
  # DELETE /celulares/1.json
  def destroy
    @celulares.destroy
    respond_to do |format|
      format.html { redirect_to celulares_url, notice: 'Celulares was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_celulares
      @celulares = Celulare.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def celulares_params
      params.require(:celulare).permit(:title, :location, :description, :invoke, :active_record)
    end
end
